<?php
require("animal.php");
require("Ape.php");
require("Frog.php");

$sheep = new Animal("shaun");

echo "Name : ". $sheep->name. "<br>"; // "shaun"
echo "legs : ".$sheep->legs. "<br>"; // 4
echo "cold blooded : ".$sheep->cold_blooded."<br>"."<br>"; // "no"

$kodok = new frog("buduk");
echo "Name : ". $kodok->name. "<br>"; // "shaun"
echo "legs : ".$kodok->legs. "<br>"; // 4
echo "cold blooded : ".$kodok->cold_blooded."<br>"; // "no"
echo "Jump : ".$kodok->jump()."<br>"."<br>"; // "hop hop"

$sungokong = new ape("kera sakti");
echo "Name : ". $sungokong->name. "<br>"; // "shaun"
echo "legs : ".$sungokong->legs. "<br>"; // 4
echo "cold blooded : ".$sungokong->cold_blooded."<br>"; // "no"
echo "Yell : ".$sungokong->yell()."<br>"; // "Auooo"
?>
